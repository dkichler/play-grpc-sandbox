import play.grpc.gen.scaladsl.PlayScalaServerCodeGenerator
import sbt.inConfig
import sbtprotoc.ProtocPlugin.autoImport.PB

name := """play-grpc-sandbox"""
organization := "org.dgkich"

version := "1.0-SNAPSHOT"
scalaVersion := "2.13.5"

lazy val Ui = config("ui").extend(Compile).describedAs("Config for UI builds")

lazy val grpcWebGen = PB.gens.plugin(
  name="grpc-web",
  path="/usr/local/bin/protoc-gen-grpc-web"
)

lazy val grpcTsWebGen = PB.gens.plugin(
  "ts_proto",
  path="./ui/node_modules/.bin/protoc-gen-ts_proto"
)


val AkkaVersion = "2.6.20"
val AkkaHttpVersion = "10.2.9"
lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .enablePlugins(AkkaGrpcPlugin)
  .enablePlugins(PlayAkkaHttp2Support)
  .configs(Ui)
  .settings(
    inConfig(Ui)(ProtocPlugin.globalSettings),
    inConfig(Ui)(ProtocPlugin.protobufConfigSettings),
    Ui / PB.protoSources := (Compile / PB.protoSources).value,
    Ui / PB.protoSources += (Compile / PB.externalIncludePath).value,
    clean := (clean dependsOn (Ui / clean)).value,
    Compile / PB.generate := ((Compile / PB.generate) dependsOn (Ui / PB.generate)).value,
    Ui / clean := sbt.io.IO.delete((Ui/cleanFiles).value),
    Ui / cleanFiles ++= Seq(
      (ThisBuild / baseDirectory).value / "ui" / "src" / "sbt-proto",
      (ThisBuild/ baseDirectory).value / "ui" / "src" / "sbt-protots",
    ),
    Ui / PB.targets ++= {
      val jsOutputDir = (ThisBuild / baseDirectory).value / "ui" / "src" / "sbt-proto"
      val tsOutputDir = (ThisBuild/ baseDirectory).value / "ui" / "src" / "sbt-protots"
      Seq(
//        (PB.gens.js, Seq("import_style=commonjs")) -> jsOutputDir,
//        (grpcWebGen, Seq("import_style=commonjs", "mode=grpcwebtext")) -> jsOutputDir,
//        (PB.gens.js, Seq("import_style=commonjs")) -> tsOutputDir,
//        (grpcWebGen, Seq("import_style=typescript", "mode=grpcwebtext")) -> tsOutputDir,
        (grpcWebGen, Seq("import_style=commonjs", "mode=grpcwebtext")) -> tsOutputDir,
        (grpcTsWebGen, Seq("import_style=commonjs,binary", "esModuleInterop=true", "outputClientImpl=grpc-web")) -> tsOutputDir
      )
    },
    akkaGrpcExtraGenerators += PlayScalaServerCodeGenerator,
    libraryDependencies ++= Seq(
      guice,
      ws,
      "com.lightbend.play" %% "play-grpc-runtime" % "0.9.1",
      "ch.megard" %% "akka-http-cors" % "1.1.3",
      "com.typesafe.akka" %% "akka-discovery" % AkkaVersion,
      "com.typesafe.akka" %% "akka-http-spray-json" % AkkaHttpVersion,
      "com.typesafe.akka" %% "akka-http2-support" % AkkaHttpVersion,
      "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
      "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test
    )
  )
