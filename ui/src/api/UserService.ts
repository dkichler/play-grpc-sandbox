import {
  GrpcWebImpl, UserService, UserServiceClientImpl,
} from '@/sbt-protots/user-service';

// pointing at Play router serving grpc over http2.  Must be proxied
const grpcWeb = new GrpcWebImpl('http://localhost:9000', {});
const userService: UserService = new UserServiceClientImpl(grpcWeb);

export default {
  userService,
};
