import {
  CurrencyService,
  CurrencyServiceClientImpl,
  GrpcWebImpl,
} from '@/sbt-protots/currency-service';

// pointing at Play router served grpc-web wrapped handler
const playGrpcWeb = new GrpcWebImpl('http://localhost:9000', {});
const playCurrencyService: CurrencyService = new CurrencyServiceClientImpl(playGrpcWeb);

// pointing at Akka http router serving grpc-web wrapped handler
const akkaGrpcWeb = new GrpcWebImpl('http://localhost:8081', {});
const akkaCurrencyService: CurrencyService = new CurrencyServiceClientImpl(akkaGrpcWeb);

export default {
  playCurrencyService,
  akkaCurrencyService,
};
