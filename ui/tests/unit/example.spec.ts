import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import ContainerSearch from '@/components/CurrencySearch.vue';

describe('CurrencySearch.vue', () => {
  it('renders properly', () => {
    const msg = 'Container';
    const wrapper = shallowMount(ContainerSearch, {});
    expect(wrapper.text()).to.include(msg);
  });
});
