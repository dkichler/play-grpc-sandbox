addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.8.18")
addSbtPlugin("com.lightbend.akka.grpc" % "sbt-akka-grpc" % "2.1.6") // last version before Akka 2.7, which is 2.2.0
addSbtPlugin("org.scalameta"     % "sbt-scalafmt"        % "2.4.0")
resolvers += Resolver.bintrayRepo("playframework", "maven")
libraryDependencies += "com.lightbend.play" %% "play-grpc-generators" % "0.9.1"
