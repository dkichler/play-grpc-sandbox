object FrontendCommands {
  val dependencyInstall: String = "npm install"
  val protoDir: String = "mkdir ./src/protots"
  val proto: String = "npm run proto"
  val test: String = "npm run test:unit"
  val serve: String = "npm run start"
  val build: String = "npm run build"

  val cleanProtoTs = "rm -Rf ./src/protots"
}
