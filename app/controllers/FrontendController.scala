package controllers

import com.google.inject.{Inject, Singleton}
import play.api.http.HttpEntity
import play.api.libs.ws.{WSClient, WSResponse}
import play.api.mvc._
import play.api.{ConfigLoader, Configuration}

import scala.concurrent.{ExecutionContext, Future}

/**
  * Frontend controller managing all static resource associate routes.
  * @param assets Assets controller reference.
  * @param cc Controller components reference.
  */
@Singleton
class FrontendController @Inject() (assets: Assets, config: Configuration, cc: ControllerComponents, ws: WSClient)(
  implicit ec: ExecutionContext
) extends AbstractController(cc) {
  import FrontendController.response2Result

  def getConf[T](name: String, default: T)(implicit loader: ConfigLoader[T]): T =
    config.getOptional[T](name).getOrElse(default)

  def index: Action[AnyContent] =
    if (getConf("production", false)) {
      assets.at("index.html")
    } else Action.async(implicit request => ws.url("http://localhost:8080/").get())

  /**
    * Serves static front-end assets, and proxies from the local npm server when in development mode.
    * @param resource The URI of a file or asset.
    * @return
    */
  def assets(resource: String): Action[AnyContent] =
    if (getConf("production", false)) {
      assets.at(resource)
    } else Action.async(implicit request => ws.url(s"http://localhost:8080/$resource").get())
}

object FrontendController {
  implicit def response2Result(futureResponse: Future[WSResponse])(implicit ec: ExecutionContext): Future[Result] =
    futureResponse map { response =>
      // we want to read the raw bytes for the body
      // Content stream is an enumerator. It's a 'stream' that generates Array[Byte]
      new Result(
        new ResponseHeader(response.status),
        HttpEntity.Streamed(response.bodyAsSource, None, Some(response.contentType))
      ).withHeaders(response.headers.mapValues(_.head).toSeq: _*)
    }
}
