package controllers

import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents}

import javax.inject.{Inject, Singleton}
import scala.concurrent.Future

@Singleton
class HealthCheckController @Inject() (
  val controllerComponents: ControllerComponents)
    extends BaseController {

  def ready: Action[AnyContent] = Action.async { _ =>
    Future.successful(Ok("Ready"))
  }
}
