package router

import akka.actor.ActorSystem
import akka.grpc.scaladsl.WebHandler
import com.google.inject.{Inject, Singleton}
import dgkich.user.grpc.{AbstractUserServiceRouter, UserProfile, UserProfileReply, UserRequest}

import scala.concurrent.Future

@Singleton
class UserServiceImpl @Inject()(implicit actorSystem: ActorSystem)
  extends AbstractUserServiceRouter(actorSystem) {

  /**
   * This router serves plain grpc over http2 and must be proxied via something like envoy...
   *
   * or must it?  experiments show this handles http1.1/grpc-web requests from the browser no problem.
   * Maybe AbstractUserServiceRouter handles it under the covers somehow?
   */
  def handler = this.respond

  val users: collection.mutable.Map[String, UserProfile] = collection.mutable.HashMap[String, UserProfile]()

  def getProfile(in: UserRequest): Future[UserProfileReply] = {
    Future.successful(UserProfileReply(users(in.username)))
  }

  def createProfile(in: UserProfile): Future[UserProfileReply] = users.get(in.username) match {
    case None => users.put(in.username, in)
      Future.successful(UserProfileReply(in))
    case Some(username) => throw new IllegalArgumentException(s"Username ${username} already exists")
  }
}
