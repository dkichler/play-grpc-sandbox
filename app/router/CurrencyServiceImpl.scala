package router

import akka.actor.ActorSystem
import akka.grpc.scaladsl.WebHandler
import com.google.inject.{Inject, Singleton}
import dgkich.currency.grpc.{AbstractCurrencyServiceRouter, Currency, CurrencyReply, CurrencyRequest}

import scala.concurrent.Future
import scala.util.Random

@Singleton
class CurrencyServiceImpl @Inject() (implicit actorSystem: ActorSystem)
  extends AbstractCurrencyServiceRouter(actorSystem) {

  override def getCurrency(in: CurrencyRequest): Future[CurrencyReply] =
    Future.successful(CurrencyReply(Some(Currency(in.currencyCode, in.currencyCode, Random.between(1,500)))))

  /**
   * This router is wrapped in a grpc-web handler, serving grpc-web directly
   */
  def handler = WebHandler.grpcWebHandler { case req =>
    this.respond(req)
  }
}
