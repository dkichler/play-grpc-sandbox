package service

import akka.actor.ActorSystem
import akka.grpc.scaladsl.WebHandler
import akka.http.scaladsl.Http
import com.google.inject.{Inject, Singleton}
import play.api.Logging
import router.CurrencyServiceImpl

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

/**
 * This class serves the router over plain akka http on port 8081
 */
@Singleton
class CurrencyServiceHttp @Inject()(
  serviceRouter: CurrencyServiceImpl
)(implicit system: ActorSystem,
  ec: ExecutionContext) extends Logging {

  val webHandlerHelloRouter = WebHandler.grpcWebHandler { case req =>
    serviceRouter.handler(req)
  }

  Http()(system).newServerAt(
      interface = "127.0.0.1",
      port = 8081
    )
    .bind(webHandlerHelloRouter)
    .onComplete {
      case Success(_) =>
        println("gRPC-Web interface bound to 127.0.0.1:8081")
        logger.info("gRPC-Web interface bound to 127.0.0.1:8081")
      case Failure(exception) =>
        println(s"Failed to bind gRPC-Web interface: $exception")
        logger.error(s"Failed to bind gRPC-Web interface: $exception")
    }
}
