package service

import com.google.inject.AbstractModule

class GrpcWebStartupModule extends AbstractModule {
  override def configure() = {
    bind(classOf[CurrencyServiceHttp]).asEagerSingleton()
  }
}
